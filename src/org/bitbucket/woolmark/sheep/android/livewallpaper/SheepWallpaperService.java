package org.bitbucket.woolmark.sheep.android.livewallpaper;

import java.util.Random;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

/**
 * android sheep wallpaper service.
 *
 * @author takimura
 */
public class SheepWallpaperService extends WallpaperService {

	/**
	 * preference:sheep.
	 */
	private static final String PREF_SHEEP = "sheep";

	/**
	 * KEY:sheep number.
	 */
	private static final String KEY_SHEEP_NUMBER = "sheep_number";

    /**
     * create random number.
     */
    private Random rand;

    /**
     * the sheep action.
     */
    private int action = 0;

    /**
     * the sheep number.
     */
    private int sheep_number;

    /**
     * the sheep image.
     */
    private Bitmap[] sheep_image;

    /**
     * the frame image.
     */
    private Bitmap background;

    /**
     * the sheep position.
     *
     * [i][0]: x pos
     * [i][1]: y pos
     * [i][2]: 0: not jumped, 1: jumped
     * [i][3]: pos of starting jump
     */
    private int[][] sheep_pos = new int[100][4];

    /**
     * add sheep flag.
     */
    private boolean add_sheep = false;

    /**
     * pause flag.
     */
    //private boolean paused = false;

    /**
     * scale.
     */
    private int scale = 1;

	@Override
	public Engine onCreateEngine() {

        /* create object */
        rand = new Random();

        // load sheep images
        sheep_image = new Bitmap[2];
        sheep_image[0] = BitmapFactory.decodeResource(getResources(),
                R.drawable.sheep00);
        sheep_image[1] = BitmapFactory.decodeResource(getResources(),
                R.drawable.sheep01);

        // load background image
        background = BitmapFactory.decodeResource(getResources(),
                R.drawable.background);

		return new SheepWallpaperEngine();
	}

	private class SheepWallpaperEngine extends WallpaperService.Engine implements Runnable {

		private Handler mHandler = null;

	    @Override
	    public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
	        super.onSurfaceChanged(holder, format, width, height);
	        
	    	if (width > height) {
	            scale = height / 200;
	    	} else {
	            scale = width / 200;
	    	}
	        
	        // reset all of sheep
	        for (int i = 0; i < sheep_pos.length; i++) {
	            sheep_pos[i][1] = -1;
	        }
	    	addSheep(0);
	    }

	    @Override
	    public void run() {
	        calc();
	        Canvas canvas = getSurfaceHolder().lockCanvas();
	        if (canvas != null) {
		        draw(canvas);
		        try {
			        getSurfaceHolder().unlockCanvasAndPost(canvas);
		        } catch (IllegalArgumentException e) {
		        	// when the device is rotated, canvas is destroyed and is unable to unlock
		        	// that exception should be ignored.
		        }
	        }

	        if (isVisible()) {
		        mHandler = new Handler(getMainLooper());
		        mHandler.postDelayed(this, 1000 / 20);
	        }
	    }

	    @Override
	    public void onVisibilityChanged(boolean visible) {
	    	super.onVisibilityChanged(visible);
	    	
	    	if (visible) {
	            // load sheep number
	            SharedPreferences prefs = getSharedPreferences(PREF_SHEEP, Context.MODE_PRIVATE);
	            if (prefs != null) {
	                sheep_number = prefs.getInt(KEY_SHEEP_NUMBER, 0);
	            }

		        mHandler = new Handler(getMainLooper());
		        mHandler.post(this);
	    	} else {
	            // save sheep number
		        SharedPreferences prefs = getSharedPreferences(PREF_SHEEP, Context.MODE_PRIVATE);
		        if (prefs != null) {
		            prefs.edit()
		                .putInt(KEY_SHEEP_NUMBER, sheep_number)
		                .commit();
		        }
		        
		        mHandler.removeCallbacks(this);
	    	}
	    }

	    public void draw(final Canvas canvas) {
	        Paint paint = new Paint();
	        paint.setAntiAlias(false);
	        paint.setFilterBitmap(false);

	        // fill background as green
	        canvas.drawColor(Color.rgb(120, 255, 120));

	        // fill sky color as blue
	        paint.setColor(Color.rgb(150, 150, 255));
	        canvas.drawRect(0, 0, getWidth(),
	                getHeight() - (background.getHeight() - 10) * scale, paint);

	        // draw fence
	        Matrix matrix = new Matrix();
	        matrix.setScale(scale, scale);
	        matrix.postTranslate(
	        		(getWidth() - background.getWidth() * scale) / 2,
	        		getHeight() - background.getHeight() * scale);
	        canvas.drawBitmap(background, matrix, paint);

	        // draw sheeps
	        for (int l = 0; l < sheep_pos.length; l++) {
	            if (sheep_pos[l][1] < 0) {
	            	continue;
	            }

	        	matrix = new Matrix();
	            matrix.setScale(scale, scale);
	            matrix.postTranslate(sheep_pos[l][0], getHeight() - sheep_pos[l][1]);
	            canvas.drawBitmap(sheep_image[action], matrix, paint);
	        }

	        // draw count
	        paint.setColor(Color.BLACK);
	        paint.setTextSize(16.0f * getResources().getDisplayMetrics().density);
	        canvas.drawText(
	        		getString(R.string.sheep_count, sheep_number),
	        		2 * scale, 2 * scale + paint.getTextSize() + getStatusBarHeight(), paint);

	    }

	    @Override
	    public void onTouchEvent(final MotionEvent event) {
	        switch(event.getAction()) {
	        case MotionEvent.ACTION_DOWN:
	            add_sheep = true;
	            return;
	        case MotionEvent.ACTION_UP:
	            add_sheep = false;
	            return;
	        }
	        super.onTouchEvent(event);
	    }

	    /**
	     * calculate.
	     */
	    private void calc() {

	        // add a sheep per a frame
	        if (add_sheep) {
	            addSheep();
	        }

	        // run the sheep
	        for (int i = 0; i < sheep_pos.length; i++) {
	            if (sheep_pos[i][1] < 0) {
	            	continue;
	            }

	            // move to left
	            sheep_pos[i][0] -= 5 * scale;

	            //
	            // jump
	            //

	            if (sheep_pos[i][0] > sheep_pos[i][3]
	            		&& sheep_pos[i][0] < sheep_pos[i][3] + sheep_image[0].getWidth() * scale * 1.5) {
	                sheep_pos[i][1] += 3 * scale;
	            } else if (sheep_pos[i][0] < sheep_pos[i][3]
	            		&& sheep_pos[i][0] > sheep_pos[i][3] - sheep_image[0].getWidth() * scale * 1.5) {
	                sheep_pos[i][1] -= 3 * scale;

	                if (sheep_pos[i][2] == 0) {
	                    sheep_number++;
	                    sheep_pos[i][2] = 1;
	                }
	            }


	            //
	            // remove a frame outed sheep
	            //

	            if (sheep_pos[i][0] < sheep_image[0].getWidth() * -1) {
	                if (i == 0) {
	                	addSheep(0);
	                } else {
	                    sheep_pos[i][1] = -1;
	                }
	            }

	        }

	        action = 1 - action;

	    }

	    private int getWidth() {
	    	return getSurfaceHolder().getSurfaceFrame().width();
	    }
	    
	    private int getHeight() {
	    	return getSurfaceHolder().getSurfaceFrame().height();
	    }
	    
	    private int getStatusBarHeight() {
	        int result = 0;
	        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
	        if (resourceId > 0) {
	            result = getResources().getDimensionPixelSize(resourceId);
	        }
	        return result;
	    }

	    /**
	     * add sheep.
	     */
	    private void addSheep() {

	        for (int i = 1; i < sheep_pos.length; i++) {
				if (sheep_pos[i][1] < 0) {
					addSheep(i);
					return;
	            }
	        }

	    }

	    /**
	     * add sheep number.
	     * @param num sheep number
	     */
	    private void addSheep(final int num) {
	        sheep_pos[num][0] = getWidth();
	        sheep_pos[num][1] = rand.nextInt((background.getHeight() - 15) * scale) + sheep_image[0].getHeight() * scale;
	        sheep_pos[num][2] = 0;
	        sheep_pos[num][3] = getJumpX(sheep_pos[num][1]);
	    }

	    /**
	     * get jump x position.
	     * @param y y position
	     * @return x position
	     */
	    private int getJumpX(final int y) {
	        return getWidth() / 2
	        		- background.getWidth() * scale / 2
	        		+ 3 * (y - sheep_image[0].getHeight() * scale) / 4
	        		- 10 * scale;
	    }

	}

}
